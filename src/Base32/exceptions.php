<?php

namespace Base32;

class InvalidCharactersException extends \Exception {

    protected $message = 'Invalid characters in the base32 string.';

}
