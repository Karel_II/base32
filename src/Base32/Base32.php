<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Base32;

/**
 * Description of Base32
 *
 * @author karel.novak
 */
class Base32 {

    protected static $lookUpTable = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', //  7
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 15
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 23
        'Y', 'Z', '2', '3', '4', '5', '6', '7', // 31
    );
    protected static $paddingChar = '='; // padding char

    /**
     * 
     * @param string $b32
     * @throws InvalidCharactersException
     */

    protected static function validateBase32($b32) {
        $match = array();
        if (!preg_match('/^(?P<base32>[' . join(static::$lookUpTable) . ']+)(([' . static::$paddingChar . ']{0,1})|([' . static::$paddingChar . ']{3})|([' . static::$paddingChar . ']{4})|([' . static::$paddingChar . ']{6}))$/', $b32, $match)) {
            throw new InvalidCharactersException();
        }
        return $match['base32'];
    }

    private static function paddingBase32($binaryStringLength) {
        switch ($binaryStringLength % 40) {
            case 8:
                return str_repeat(static::$paddingChar, 6);
            case 16:
                return str_repeat(static::$paddingChar, 4);
            case 24:
                return str_repeat(static::$paddingChar, 3);
            case 32:
                return str_repeat(static::$paddingChar, 1);
        }
        return '';
    }

    /**
     * Helper class to decode base32
     *
     * @param string $data
     * @return bool|string
     */
    public static function base32_decode($data) {
        $data = self::validateBase32($data);
        $base32charsFlipped = array_flip(static::$lookUpTable);

        $n = 0;
        $j = 0;
        $binary = "";

        for ($i = 0; $i < strlen($data); $i++) {

            $n = $n << 5;     // Move buffer left by 5 to make room
            $n = $n + $base32charsFlipped[$data[$i]];  // Add value into buffer
            $j = $j + 5;    // Keep track of number of bits in buffer
            if ($j >= 8) {
                $j = $j - 8;
                $binary .= chr(($n & (0xFF << $j)) >> $j);
            }
        }
        return $binary;
    }

    /**
     * Helper class to encode base32
     *
     * @param string $data
     * @param bool $padding
     * @return string
     */
    public static function base32_encode($data, $padding = true) {
        if (empty($data)) {
            return '';
        }
        $base32chars = static::$lookUpTable;
        $n = 0;
        $j = 0;
        $binary = "";
        for ($i = 0; $i < strlen($data); $i++) {
            $n = $n << 8;
            $n = $n + ord($data[$i]);
            $j = $j + 8;
            while ($j >= 5) {
                $j = $j - 5;
                $binary .= $base32chars[(($n & (0x1F << $j)) >> $j)];
            }
            if ($j > 0 && ($i + 1) == strlen($data)) { //rest of bites
                $n = $n << ($j % 5);
                $binary .= $base32chars[($n & (0x1F))];
            }
        }
        if ($padding) {
            $binary .= static::paddingBase32(strlen($data) * 8);
        }
        return $binary;
    }

    /**
     * 
     * @param int $length
     * @return array
     */
    public static function base32_generate($length = 16) {
        $return = '';
        $rand_keys = array_rand(static::$lookUpTable, $length);
        shuffle($rand_keys);
        foreach ($rand_keys as $key) {
            $return .= static::$lookUpTable[$key];
        }
        return $return;
    }

}
