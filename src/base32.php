<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        'Base32\\InvalidCharactersException' => 'exceptions.php',
        'Base32\\Base32' => 'Base32.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/Base32/' . $classMap[$className];
    }
});
